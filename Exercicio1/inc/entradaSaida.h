//Trabalho 1 - Fundamentos de Sistemas Operacionais
//Exercício 1
//Arquivo entradaSaida.h
//Aluno: João Paulo Nunes Soares
//Matrícula: 15/0038267
 #include "tiposCompostos.h"

#ifndef OPERACOESGEOMETRICAS_H
#define OPERACOESGEOMETRICAS_H

//Prints convex polygon and his area
void printResultConvex(double area);

//Checks if polygon is convex
void doCalculus(struct Quadrilateral quadrilateral);

//Receives the values of the entered coordinates
void receivesValues();


#endif
