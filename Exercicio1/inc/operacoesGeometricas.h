//Trabalho 1 - Fundamentos de Sistemas Operacionais
//Exercício 1
//Arquivo operacoesGeometricas.h
//Aluno: João Paulo Nunes Soares
//Matrícula: 15/0038267
 #include "tiposCompostos.h"

#ifndef OPERACOESGEOMETRICAS_H
#define OPERACOESGEOMETRICAS_H

// Calculates the convex polygon area
double PolygonArea(Quadrilateral *polygon,int N);

// To find orientation of ordered triplet (p, q, r).
// The function returns following values
// 0 --> p, q and r are colinear
// 1 --> Clockwise
// 2 --> Counterclockwise
int orientation(Point p,Point q,Point r);

// Calculates convex hull of a set of n points.
// Returns:
//      1 -> Not Convex Polygon
//      2 -> Convex Polygon
int convexHull(Quadrilateral points [],int n);


#endif
