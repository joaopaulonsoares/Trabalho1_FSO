//Trabalho 1 - Fundamentos de Sistemas Operacionais
//Exercício 1
//Arquivo operacoesGeometricas.c
//Aluno: João Paulo Nunes Soares
//Matrícula: 15/0038267

#include <stdio.h>
#include <stdlib.h>

#include "operacoesGeometricas.h"
#include "tiposCompostos.h"

/*Algoritmo retirado de: http://paulbourke.net/geometry/polygonmesh/source1.c
  Adaptações foram feitas
*/
double PolygonArea(Quadrilateral *polygon,int N){
   int i,j;
   double area = 0;

   for (i=0;i<N;i++) {
      j = (i + 1) % N;
      area += polygon[i].coord.x * polygon[j].coord.y;
      area -= polygon[i].coord.y * polygon[j].coord.x;
   }

   area /= 2;
   return(area < 0 ? -area : area);
}



int orientation(Point p, Point q, Point r){
    double val = (q.y - p.y) * (r.x - q.x) - (q.x - p.x) * (r.y - q.y);
    if (val == 0){
      return 0; // colinear
    }else if (val > 0) {
      return 1;
    }else{
      return 2;
    }
}

int convexHull(Quadrilateral points[], int n){

    // There must be at least 3 points
    if (n < 3){
        return 1;
    }

    // Initialize Result
    double next[n];
    for (int i = 0; i < n; i++){
        next[i] = -1;
    }

    // Find the leftmost point
    int l = 0, teste=0;
    for (int i = 1; i < n; i++){
        if (points[i].coord.x < points[l].coord.x)
            l = i;
    }
    // Start from leftmost point, keep moving counterclockwise
    // until reach the start point again
    int p = l, q;
    do{
        // Search for a point 'q' such that orientation(p, i, q) is
        // counterclockwise for all points 'i'
        q = (p + 1) % n;
        for (int i = 0; i < n; i++)
            if (orientation(points[p].coord, points[i].coord, points[q].coord) == 2)
                q = i;

        next[p] = q; // Add q to result as a next point of p
        p = q; // Set p as q for next iteration
    } while (p != l);

    // Print Result
    for (int i = 0; i < n; i++){
        if (next[i] != -1){
            	//printf("(%lf,%lf)\n", points[i].coord.x,points[i].coord.y );
            teste++;
        }
    }

    if(teste<4){
  		//printf("Não convexo\n");
      return 1;
    }else{
  		//printf("Convexo\n");
      //PolygonArea(points,4);
      return 2;
    }

}
