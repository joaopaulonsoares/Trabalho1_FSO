
SISTEMA OPERACIONAL

	O sistema operacional utilizado foi O linux, com a distro Mint versão 18.0 .


AMBIENTE DE DESENVOLVIMENTO

	Como editor de texto foi utilizado o Atom, e para compilar o programa o compilador gcc.


INSTRUÇÕES DE USO

	Exercício 1
		1. Após entrar na pasta do exercício, execute o comando make clean .
		2. Para compilar o arquivo e gerar o executável , execute o comando make .
		3. Para executar o programa execute o binário gerado com o nome main , para fazer isso execute o comando ./main .
		4. Insira os valores de entrada para o exercício.

	Exercício 2
		1. Primeiro entre na pasta do exercício 2.
		2. Para compilar o arquivo e gerar o executável . Execute:
							gcc main.c -o main 
		3. Para executar o programa execute o binário gerado com o nome main , para fazer isso execute o comando ./main  (parametros).
		Onde parametros, corresponde aos parâmetros que se deseja passar para a execução do exercício.Exemplo:
							./main var1 var2 var3

	Exercício 3
		1. Após entrar na pasta do exercício, execute o comando make clean .
		2. Para compilar o arquivo e gerar o executável , execute o comando make .
		3. Para executar o programa execute o binário gerado com o nome main , para fazer isso execute o comando ./main (parametro) .Onde parametro corresponde a variavel que determina o formato da ordenação.
		4. Insira os valores de entrada para o exercício.
		

LIMITAÇÕES CONHECIDAS

	Exercício 1
		→Caso o usuário insira caracteres em vez dos números, o programa não recebe nenhuma outra variável e indica que o quadrilatero é não convexo

	Exercício 2
		→Não há limitações conhecidas.

	Exercício 3
		→Caso o usuário digite um caractere em vez dos números, o programa entra em um loop infinito.Assim, recebendo variáveis sem respeitar a condição de parada.